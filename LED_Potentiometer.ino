


int pot1 = A0; //Potentiometer1 Pin
int pot2=A1; //Potentiometer2 Pin

int LED[12]; //Array to store 12 LEDs
int val1; //Potentiometer 1 value
int val2; //Potentiometer 2 value

void setup() {
  pinMode(pot1, INPUT); //Define potentiometer as an analog input
  pinMode(pot2,INPUT); //Define potentiometer as an analog input

  int p;
  int j;
//Define each element of the array as an Arduino pin from pin 2 to 11
  for (p = 0; p < 12; p = p + 1) {
    LED[p] = p + 2;
  }
//Define each pin as output
  for (j = 0; j < 12; j++) {
    pinMode(LED[j], OUTPUT);
  }
}

void loop() {
  val1 = analogRead(pot1);  //Read the value pot1
  val2 = analogRead(pot2); //Read the value pot1
  val1 = map(val1, 0, 1023, 0, 220);//map the value to 220
  val2 = map(val2,0,1023,0,220);//map the value to 220

//Each for loop will setup the ammount of LEDs on or off
  if (val1 == 0) {
    for (int i = 0; i < 12; i++) {
      digitalWrite(LED[i], LOW);
    }
  }
  
  if (val1 > 0 && val1 < 20) {
    digitalWrite(LED[0], HIGH);
    analogWrite(LED[0],val2);
    
    for (int i = 1; i < 12; i++) {
      digitalWrite(LED[i], LOW);
    }
  }


  if ( val1 > 20 && val1 < 40) {
    for (int i = 0; i < 2; i++) {
      digitalWrite(LED[i], HIGH);
      analogWrite(LED[i],val2);
    }
    for (int i = 2; i < 12; i++) {
      digitalWrite(LED[i], LOW);
    }
  }

  if ( val1 > 20 && val1 < 40) {
    for (int i = 0; i < 3; i++) {
      digitalWrite(LED[i], HIGH);
      analogWrite(LED[i],val2);
    }
    for (int i = 3; i < 12; i++) {
      digitalWrite(LED[i], LOW);
    }
  }

  if ( val1 > 40 && val1 < 60) {
    for (int i = 0; i < 4; i++) {
      digitalWrite(LED[i], HIGH);
      analogWrite(LED[i],val2);
    }
    for (int i = 4; i < 12; i++) {
      digitalWrite(LED[i], LOW);
    }
  }

  if ( val1 > 60 && val1 < 80) {
    for (int i = 0; i < 5; i++) {
      digitalWrite(LED[i], HIGH);
      analogWrite(LED[i],val2);
    }
    for (int i = 5; i < 12; i++) {
      digitalWrite(LED[i], LOW);
    }
  }

  if ( val1 > 80 && val1 < 100) {
    for (int i = 0; i < 6; i++) {
      digitalWrite(LED[i], HIGH);
      analogWrite(LED[i],val2);
    }
    for (int i = 6; i < 12; i++) {
      digitalWrite(LED[i], LOW);
    }
  }

  if ( val1 > 100 && val1 < 120) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(LED[i], HIGH);
      analogWrite(LED[i],val2);
    }
    for (int i = 7; i < 12; i++) {
      digitalWrite(LED[i], LOW);
    }
  }

  if ( val1 > 120 && val1 < 140) {
    for (int i = 0; i < 8; i++) {
      digitalWrite(LED[i], HIGH);
      analogWrite(LED[i],val2);
    }
    for (int i = 8; i < 12; i++) {
      digitalWrite(LED[i], LOW);
    }
  }

  if ( val1 > 140 && val1 < 160) {
    for (int i = 0; i < 9; i++) {
      digitalWrite(LED[i], HIGH);
      analogWrite(LED[i],val2);
    }
    for (int i = 9; i < 12; i++) {
      digitalWrite(LED[i], LOW);
    }
  }

  if ( val1 > 160 && val1 < 180) {
    for (int i = 0; i < 10; i++) {
      digitalWrite(LED[i], HIGH);
      analogWrite(LED[i],val2);
    }
    for (int i = 10; i < 12; i++) {
      digitalWrite(LED[i], LOW);
    }
  }

  if ( val1 > 180 && val1 < 200) {
    for (int i = 0; i < 11; i++) {
      digitalWrite(LED[i], HIGH);
      analogWrite(LED[i],val2);
    }
    digitalWrite(LED[11], LOW);
  }



  if ( val1 > 200 && val1 < 220) {
    for (int i = 0; i < 12; i++) {
      digitalWrite(LED[i], HIGH);
      analogWrite(LED[i],val2);
    }
  }
}
